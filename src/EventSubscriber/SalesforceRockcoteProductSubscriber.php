<?php

namespace Drupal\rockcote_salesforce\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\salesforce\Event\SalesforceEvents;
use Drupal\salesforce_mapping\Event\SalesforceQueryEvent;
use Drupal\salesforce_mapping\Event\SalesforcePullEvent;
use Drupal\salesforce_mapping\Event\SalesforcePullEntityValueEvent;

/**
 * Class SalesforceRockcoteProductSubscriber. Modification of Salesforce object.
 */
class SalesforceRockcoteProductSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a DiffBuilderManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * Modification of Salesforce query object.
   */
  public function pullQuery(SalesforceQueryEvent $event) {
    // $mapping = $event->getMapping();.
    $query = $event->getQuery();
    $query->order = [
      // Or use LastModifiedDate, Name or Parent_Family__c.
      'CreatedDate' => 'ASC',
    ];
    $this->logger->notice('pullQuery');
  }

  /**
   * Modification of Salesforce object.
   */
  public function pullPrepull(SalesforcePullEvent $event) {
  }

  /**
   * Modification of Salesforce object.
   */
  public function pullPresave(SalesforcePullEvent $event) {
    // $mapping = $event->getMapping();.
    $mapped_object = $event->getMappedObject();
    $entity = $event->getEntity();
    // $fields = $mapping->getPullFields();.
    $sf_record = $mapped_object->getSalesforceRecord();
    $family_sfid = NULL;

    $this->logger->notice('pullPresave');

    // Nodes import.
    if (in_array($entity->getType(), [
      'rockcote_product',
      'rockcote_tds',
      'rockcote_sds',
    ])) {
      $family_sfid_field = ($entity->getType() === 'rockcote_product') ? 'Parent_Family__c' : 'Product_Family__c';
      $family_sfid = $sf_record->field($family_sfid_field);
    }

    $this->logger->notice('pullPresave');

    if ($entity instanceof NodeInterface) {
      $this->logger
        ->notice('Node of type : @type :: @typeid', [
          '@type' => $entity->getType(),
          '@typeid' => $entity->getEntityTypeId(),
        ]);

      // Set user to admin.
      $entity->set('uid', 1);

      if ($entity->getType() === 'rockcote_product') {
        // Removing mapped URL to image.
        if (isset($entity->field_rockcote_product_image)) {
          $default_image = $this->entityTypeManager
            ->getStorage('file')
            ->load(10);
          $entity->set('field_rockcote_product_image', $default_image);
        }
      }

      $entity = $this->setParentReference($entity, $family_sfid);
      // $mapped_object->setDrupalEntity($entity);.

      // Set status.
      if (in_array($entity->getType(), [
        'rockcote_tds',
        'rockcote_sds',
      ])) {
        $entity->set('status', $sf_record->field('Status__c') === 'Active');
      }

    }
    // Paragraphs import.
    else if ($entity instanceof ParagraphInterface) {
      $salesforce_id = $sf_record->field(($entity->getType() === 'rockcote_sds_control_param') ? 'X8_1_SDS__c' : 'Safety_Data_Sheet__c');
      $entity = $this->setSdsReference($entity, $salesforce_id);
    }
  }

  /**
   * Set parent reference (if exists).
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Salesforce entity.
   * @param string $family_sfid
   *   Parent reference id.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Updated entity.
   */
  protected function setParentReference(EntityInterface $entity, $family_sfid) {

    if (isset($entity->field_rockcote_product_ref) && (strlen($family_sfid) > 0)) {
      $nodes = $this->entityTypeManager
        ->getStorage('node')
        ->loadByProperties([
          'type' => 'rockcote_product',
          'field_rockcote_salesforce_id' => $family_sfid,
        ]);

      if (count($nodes) === 1) {
        $entity->set('field_rockcote_product_ref', $this->entityTypeManager
          ->getStorage('node')
          ->load(reset($nodes)->id()));
      }
      else {
        $this->logger
          ->warning($this->t('Migration issue for parent. SSID: @ssid', ['@ssid' => $family_sfid]));
      }
    }

    return $entity;
  }

  /**
   * Set SDS reference (if exists).
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Salesforce entity.
   * @param string $salesforce_id
   *   Salesforce id.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Updated entity.
   */
  protected function setSdsReference(EntityInterface $entity, $salesforce_id) {

    if (strlen($salesforce_id) == 0) {
      $this->logger
        ->warning($this->t('Empty salesforce id.'));
    }

    if (isset($entity->field_rockcote_sds_ref)) {
      $nodes = $this->entityTypeManager
        ->getStorage('node')
        ->loadByProperties([
          'type' => 'rockcote_sds',
          'field_rockcote_salesforce_id' => $salesforce_id,
        ]);

      if (count($nodes) === 1) {
        $entity->set('field_rockcote_sds_ref', $this->entityTypeManager
          ->getStorage('node')
          ->load(reset($nodes)->id()));
      }
      else {
        $this->logger
          ->warning($this->t('Migration issue for finding SDS with id @ssid', ['@ssid' => $salesforce_id]));
      }
    }

    return $entity;
  }

  /**
   * Modification of Salesforce object.
   */
  public function pullEntityValue(SalesforcePullEntityValueEvent $event) {
    // $this->logger->notice('pullEntityValue');.
    // $entity = $event->getEntity();.
    // $entityValue = $event->getEntityValue();.
    // $this->logger->warning($entityValue);.
    // $field = $event->getFieldPlugin();.
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      SalesforceEvents::PULL_QUERY => 'pullQuery',
      SalesforceEvents::PULL_PREPULL => 'pullPrepull',
      SalesforceEvents::PULL_PRESAVE => 'pullPresave',
      SalesforceEvents::PULL_ENTITY_VALUE => 'pullEntityValue',
    ];

    return $events;
  }

}
