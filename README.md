# ROCKCOTE salesforce

The modules migrates date from the following sources:

* Products, SDS and TDS from `Salesforce`

Module depends on

* [Salesforce Suite](https://www.drupal.org/project/salesforce)
* [ROCKCOTE structure common](https://bitbucket.org/rockcote/rockcote_structure_common)

## Installation

Add the following lines to `repositories` section of `composer.json`

```
  ...
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rockcote/rockcote_user.git"
    },
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rockcote/rockcote_structure_common.git"
    },
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rockcote/rockcote_salesforce.git"
    }
  ],
  ...
  "extra": {
    "installer-paths": {
      ...
      "web/modules/custom/{$name}": [
        "type:drupal-module-custom"
      ],
      ...
    }
  }
  ...
```

Run composer command

```
  composer require rockcote/rockcote_salesforce
```

Additionally, you can exclude current module in `.gitignore`

```
/web/modules/custom/rockcote_structure_common
/web/modules/custom/rockcote_salesforce
/web/modules/custom/rockcote_user
```

## Updating

To update to the latest version run

```
composer update rockcote/rockcote_salesforce --with-dependencies
```

## Usage

* Authenticate in Salesforce by going to `/admin/config/salesforce/authorize`
* Run cron via `/admin/config/development/performance` interface or drush

```
drush cron
```

### Refresh items from SF

Run

```
drush sf-pull-query rockcote_product_sds
drush sf-pull-query rockcote_product_tds
drush sf-pull-query rockcote_product
drush cron
```

## Issues

Move to [bitbucket](https://bitbucket.org/rockcote/rockcote_salesforce) from [gitlab](https://gitlab.com/rockcote-aus/rockcote_salesforce) due to the [composer issue](https://github.com/composer/composer/issues/6642) and [gitlab issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/37355).

## Repositories

* [Bitbucket](https://bitbucket.org/rockcote/rockcote_salesforce): active
* [GitLab](https://gitlab.com/rockcote-aus/rockcote_salesforce): composer prevents from using GitLab as of 04 Sep 2017. See [issues](#issues).
